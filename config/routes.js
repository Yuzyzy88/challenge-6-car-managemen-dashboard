const path = require('path');
const express = require("express");
const controllers = require("../app/controllers");
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument =  YAML.load(path.join(__dirname,'..','openapi.yml'));

const appRouter = express.Router();
const apiRouter = express.Router();

/** SWAGGER */
appRouter.use('/api-docs', swaggerUi.serve);
appRouter.get('/api-docs', swaggerUi.setup(swaggerDocument));

/** FRONT */
appRouter.get("/", controllers.main.index);
appRouter.get("/add", controllers.main.add);
appRouter.get("/cars/:id", controllers.main.update);

/**
 * TODO: Implement your own API
 *       implementations
 */
apiRouter.get("/", controllers.api.v1.carController.list);
apiRouter.post("/", controllers.api.v1.auth.authorize, controllers.api.v1.auth.checkSuperOrAdmin ,controllers.api.v1.carController.create);
apiRouter.delete("/delete/:id", controllers.api.v1.auth.authorize, controllers.api.v1.auth.checkSuperOrAdmin, controllers.api.v1.carController.destroy);
apiRouter.put("/update/:id", controllers.api.v1.auth.authorize, controllers.api.v1.auth.checkSuperOrAdmin, controllers.api.v1.carController.update);

/** SUPERADMIN */
apiRouter.post("/superadmin/login", controllers.api.v1.userController.login);
apiRouter.post("/superadmin/register", controllers.api.v1.auth.authorize, controllers.api.v1.auth.checkSuperAdmin, controllers.api.v1.userController.register);

/* ADMIN */
apiRouter.post("/admin/login", controllers.api.v1.userController.login);

/** MEMBER */
apiRouter.post("/member", controllers.api.v1.userController.register);
apiRouter.post("/member/login", controllers.api.v1.userController.login);
apiRouter.get("/user/whoami", controllers.api.v1.auth.authorize, controllers.api.v1.auth.whoAmI);

appRouter.use('/api/cars',apiRouter);

module.exports = appRouter;