const { UserRoles} = require("../models");

module.exports = {
    findUserRolesByName(condition) {
        return UserRoles.findOne(condition);
    },
    findByPkRoles(id) {
        return UserRoles.findByPk(id);
    }
}