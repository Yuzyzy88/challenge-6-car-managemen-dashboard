const { users } = require("../models");

module.exports = {
    create(requestBody) {
        return users.create(requestBody);
    },

    findOne(condition) {
        return users.findOne(condition);
    },

    findByPk(id) {
        return users.findByPk(id);
    }
}