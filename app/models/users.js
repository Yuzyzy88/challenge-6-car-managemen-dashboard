'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.users, {
        foreignKey: 'userRoleId',
        as: 'userRoles'
      })
    }
  }
  users.init({
    name: DataTypes.STRING,
    role: DataTypes.STRING,
    email: DataTypes.STRING,
    encryptedPassword: DataTypes.STRING,
    userRoleId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'UserRoles',
        key: 'id'
      }
    }
  }, {
    sequelize,
    modelName: 'users',
  });
  return users;
};