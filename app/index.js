const express = require('express');
const session = require('express-session');
const expressLayouts = require('express-ejs-layouts');
const cookieParse = require('cookie-parser');
const flash = require('connect-flash');
const router = require("../config/routes");
const path = require("path");

const publicDir = path.join(__dirname, "../public");
const viewsDir = path.join(__dirname, "./views");
const app = express();

app.use(express.urlencoded({ extended: true })) // for parsing application/ x-www-form-urlencoded
app.use(expressLayouts);

/** install JSON request parser */
app.use(express.json());

/** install view engine */
app.set("views", viewsDir);
app.set('view engine', 'ejs');

/** set public directory */
app.use(express.static(publicDir));

/** configure flash */
app.use(cookieParse('secret'));
app.use(
    session({
        cookie: { maxAge: 6000 },
        secret: 'secret',
        resave: true,
        saveUninitialized: true,
    })
);
app.use(flash());

/** install router */
app.use(router)

module.exports = app;
