const carService = require("../../../services/carService");

const multer = require('multer');
const { imageFilter } = require('../../../../helpers');

// define the storage location for our images
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, __dirname + '../../../../../public/images/');
    },
    // by default, multer removes file extensions so let's add them back
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
})
const upload = multer({ storage: storage, fileFilter: imageFilter }).single('image');

async function list(req, res) {
    try {
        const data = await carService.list()
        return res.status(200).json({
            success: true,
            error: false,
            message: " Data successfully populated",
            data: data
        });
    } catch (error) {
        return res.status(400).json({
            success: false,
            error: true,
            data: null,
            message: error
        });
    }
}

async function create(req, res) {
    upload(req, res, async function (err) {
        if (err instanceof multer.MulterError) {
            console.error(err)
        } else if (err) {
            console.error(err)
        }
        try {
            const data = await carService.create({
                name: req.body.name,
                price: req.body.price,
                size: req.body.size,
                image: req.file ? req.file.originalname : "",
            })
            await carService.update(data.id, { createdBy: req.user.id })

            res.status(200).json({
                success: true,
                error: false,
                data: data,
                message: " Data successfully created"
            })
        } catch (error) {
            console.log('error', error);
            res.status(400).json({
                success: false,
                error: true,
                data: null,
                message: error
            });
        }
    })
}

async function destroy(req, res) {
    try {
        const data = await carService.delete(req.params.id)
        carService.update(req.params.id, { deletedBy: req.user.id }, false)
        console.log('user', req.user.id);
        return res.status(200).json({
            success: true,
            error: false,
            data: data,
            message: " Data successfully deleted"
        })
    } catch (err) {
        console.log(err);
        return res.status(400).json({
            success: false,
            error: true,
            data: null,
            message: err
        });
    }
}

async function update(req, res,) {
    let upload = multer({ storage: storage, fileFilter: imageFilter }).single('image');
    upload(req, res, async (err) => {
        await carService.findOne(req.params.id).then(car => {
            carService.update(
                req.params.id,
                {
                    name: req.body.name,
                    price: req.body.price,
                    size: req.body.size,
                    image: req.file ? req.file.originalname : car.image,
                    updatedBy: req.user.id
                }, false
            );
            
        });

        res.status(200).json({
            message: "Success Update"
        })
    });
}

module.exports = { create, list, destroy, update };