const carController = require("./carController");
const userController = require("./userController");
const auth = require("./auth");

module.exports = {
    carController,
    userController,
    auth
}