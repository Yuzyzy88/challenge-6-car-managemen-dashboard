const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { findByPk } = require('../../../repositories/userRepositories');
const { findByPkRoles } = require('../../../repositories/userRoleRepositories');

function encryptPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.hash(password, 6, (err, encryptPassword) => {
            if (!!err) {
                reject(err);
                return;
            };
            resolve(encryptPassword);
        });
    });
}


function comparePassword(encryptedPassword, password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, encryptedPassword, (err, isPasswordCorrect) => {
            if (!!err) {
                reject(err);
                return;
            }
            resolve(isPasswordCorrect);
        });
    });
}

function createToken(payload) {
    return jwt.sign(payload, "secret");
}

async function authorize(req, res, next) {
    try {
        const bearerToken = req.headers.authorization;
        const token = bearerToken.split("Bearer ")[1];

        const verifyTokenResult = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "secret");
        req.user = await findByPk(verifyTokenResult.id)
        next();
    } catch (error) {
        res.status(401).json({
            success: false,
            error: true,
            data: null,
            message: 'Unauthorize'
        })
    }
}

async function whoAmI(req, res) {
    res.status(200).json(req.user);
}

async function checkSuperAdmin(req, res, next) {
    try {
        const bearerToken = req.headers.authorization;
        const token = bearerToken.split("Bearer ")[1];
        const verifyTokenResult = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "secret");

        req.user = await findByPk(verifyTokenResult.id)
        const roleid = req.user.userRoleId

        if (roleid == '1') {
            next();
        } else {
            res.status(401).json({
                success: false,
                error: true,
                data: null,
                message: 'Only Admin can access'
            })
        }

    } catch (error) {
        res.status(401).json({
            success: false,
            error: true,
            data: null,
            message: error
        })
        console.log(error);
    }
}

async function checkSuperOrAdmin(req, res, next) {
    try {
        const bearerToken = req.headers.authorization;
        const token = bearerToken.split("Bearer ")[1];
        const verifyTokenResult = jwt.verify(token, process.env.JWT_SIGNATURE_KEY || "secret");

        req.user = await findByPk(verifyTokenResult.id)
        const roleid = req.user.userRoleId

        console.log('userid', roleid);
        if (roleid == '1'||'2') {
            next();
        } else {
            res.status(401).json({
                success: false,
                error: true,
                data: null,
                message: 'Only SuperAdmin and Admin can access'
            })
        }

    } catch (error) {
        res.status(401).json({
            success: false,
            error: true,
            data: null,
            message: error
        })
        console.log(error);
    }
}

module.exports = { encryptPassword, comparePassword, createToken, authorize, whoAmI, checkSuperAdmin, checkSuperOrAdmin }