const { Car } = require('../models');


module.exports = {
    index(req, res) {
        Car.findAll().then(Car => {
            res.status(200).render("index", {
                layout: 'layouts/main-layout',
                title: 'List Car',
                data: Car,
                msgs: req.flash('msg'),
            });
        });
    },

    add(req, res) {
        res.status(200).render('add', {
            layout: 'layouts/main-layout',
            title: 'Add Car'
        });
    },

    update(req, res) {
        Car.findByPk(req.params.id).then(Car => {
            res.render("update", {
                layout: 'layouts/main-layout',
                title: 'Update',
                data: Car
            });
        });
    }
}