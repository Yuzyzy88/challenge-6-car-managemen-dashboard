const carRepository = require("../repositories/carRepository");

module.exports = {
    async list() {
        const data = await carRepository.findAll();
        const count = data.length;
        return {
            data: data,
            total: count
        };
    },


    create(requestBody) {
        return carRepository.create(requestBody);
    },

    delete(id) {
        return carRepository.delete(id);
    },

    update(id, requestBody, paranoid=true) {
        return carRepository.update(id, requestBody, paranoid);
    },

    findOne(id) {
        return carRepository.find(id)
    }
}