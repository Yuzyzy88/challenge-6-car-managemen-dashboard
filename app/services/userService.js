const userRepository = require("../repositories/userRepositories");
const { encryptPassword, comparePassword, createToken } = require("../controllers/api/v1/auth");
const { userRoles } = require("../models");
const { findUserRolesByName } = require("../repositories/userRoleRepositories");

module.exports = {
    async create(requestBody) {
        const name = requestBody.body.name;
        const role = requestBody.body.role;
        const email = requestBody.body.email;
        const pass = requestBody.body.password;
        
    
        const encryptedPassword = await encryptPassword(pass);

        const roleId = await findUserRolesByName({ where: { name: role } });
        const userRoleId = roleId.id;

        if (!userRoleId) {
            return {
                data: null,
                message: 'User role does not exist'
            }
        }

        const data = userRepository.create({ name, role, email, encryptedPassword, userRoleId });
        return {
            data
        }
    },

    async checkUser(requestBody) {

        const email = requestBody.body.email;
        const password = requestBody.body.password;

        const user = await userRepository.findOne({ where: { email }, include: ['userRoles'] });

        if (!user) {
            return {
                data: null,
                message: 'User not found!'
            }
        }

        const isPasswordCorrect = await comparePassword(user.encryptedPassword, password)

        if (!isPasswordCorrect) {
            return {
                data: null,
                message: 'Password is incorrect'
            }
        }

        const token = createToken({
            id: user.id,
            name: user.name,
            role: user.userRoleId
        })
        // const role = user.userRoleId
        // console.log("role",role);

        

        return {
            data: user,
            token,
            message: 'User found'
        }
    },


}
