'use strict';

const { encryptPassword } = require("../../app/controllers/api/v1/aunth");

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

  const encryptedPassword = await encryptPassword('superadmin');
   await queryInterface.bulkInsert('users', [{
     email: 'superadmin@gmail.com',
     encryptedPassword: encryptedPassword,
     role: 'superadmin',
     name: 'ayu',
     userRoleId: 1,
     createdAt: new Date(),
     updatedAt: new Date()
   }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
