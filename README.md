# Challenge FSW Chapter 6 
## Tentang desain web ini

Desain web ini untuk memenuhi kebutuhan manajemen data mobil. Web ini dapat menambah, merubah dan menghapus (CURD) data mobil yang akan disimpan di dalam db potsgres. Dibuat dengan menggunakan:


Yang harus diinstall:
- npm install
- npm install --save-dev sequelize-cli
  
Sebelum melakukan migration dimohon untuk mengubah/mengechek inisialisasi data di config/config.js 

Migration:
- npm run db:create 
- npm run db:migrate

Dijalankan dengan command:
- npm run develop
## Info

package.json - script: written for windows